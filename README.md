Ce dépôt contient le script ayant servi à télécharger les pages du [Copyright Madness](http://www.numerama.com/tag/copyright-madness/) pour en créer un recueil à l'occation du [Ray's day](https://raysday.net) 2016.

L'epub est accessible sur <https://frama.link/CopyrightMadness_RaysDay_2016>.

Ce travail est accessible dans les termes de la WTFPL (voir le fichier [LICENSE](LICENSE)).
