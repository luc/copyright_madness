#!/usr/bin/perl
use Mojo::Base -base;
use Mojo::UserAgent;
use Mojo::Util qw(spurt encode);
use Mojo::IOLoop;
use Mojo::URL;
use Mojo::Parameters;

binmode STDOUT, ":utf8";

my $ua = Mojo::UserAgent->new();
$ua->max_redirects(3);

my $header = <<EOF;
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops">
    <head>
        <title></title>
        <link href="../Styles/Style0001.css" rel="stylesheet" type="text/css"/>
    </head>

    <body>
EOF

my $footer =<<EOF;
    </body>
</html>
EOF

my $i = 1;
while ($i <= 12) {
    $ua->get("http://www.numerama.com/tag/copyright-madness/page/$i" => sub {
            my ($ua, $tx) = @_;
            if (my $res = $tx->success) {
                my $dom = $res->dom;
                say "got page ".$tx->req->url;
                $dom->find('article > a.floatleft')->each(
                    sub {
                        my ($e, $num) = @_;
                        my $url = $e->attr->{href};
                        $ua->get($url => sub {
                                my ($ua, $tx) = @_;
                                if (my $res = $tx->success) {
                                    my $dom   = $res->dom;
                                    if ($dom->at('meta[itemprop="description"]')->attr->{content} =~ m/Lionel/) {
                                        my $date = $dom->at('meta[property="published_time"]');
                                        if (!defined($date)) {
                                            $date = $dom->at('meta[property="article:published_time"]');
                                        }
                                        $date        = $date->attr->{content};
                                        my $filename = $date;
                                        $date        =~ s#(\d{4})-(\d{2})-(\d{2}).*#<p>Publié le $3/$2/$1 sur <a href="$url">Numerama</a></p>#;
                                        $filename    =~ s#(\d{4})-(\d{2})-(\d{2}).*#$1-$2-$3.xhtml#;

                                        my $title   = $dom->at('div.post-title > h1')->text;
                                        my $content = $dom->at('article.post-content');

                                        unless ($content =~ m#storify\.com/Copyrightmad#) {
                                            $content->prepend_content("\n<h1>$title</h1>\n$date");
                                            $content->find('img')->each(
                                                sub {
                                                    my ($e, $num) = @_;
                                                    my $url = Mojo::URL->new($e->attr->{src});
                                                    $url->scheme('http');
                                                    my $name = $url;
                                                    $name->query(Mojo::Parameters->new());
                                                    $name    =~ s#.*/([^/]*)#$1#;
                                                    $e->attr->{src} = "../Images/$name";
                                                    delete $e->attr->{srcset};
                                                    open my $fh, '>>', 'Images/images.sh';
                                                    say $fh "wget $url -O $name";
                                                    close $fh;
                                                }
                                            );
                                            if ($content =~ m/<iframe/) {
                                                $content->find('iframe')->map('remove')->each;
                                            }
                                            if ($content =~ m#<script#) {
                                                $content->find('script')->map('remove')->each;
                                            }
                                            if ($content =~ m#https://t\.co#) {
                                                $content->find('a')->each(
                                                    sub {
                                                        my ($e, $num) = @_;
                                                        my $url  = $e->attr->{href};
                                                        my $text = $e->content;
                                                        if ($url =~ m#https://t\.co#) {
                                                            my $tx = $ua->get($url);
                                                            if ($tx->success) {
                                                                $e->attr(href => $tx->req->url->to_string);
                                                                if ($text =~ m#^https://t\.co#) {
                                                                    $e->content($tx->req->url->to_string);
                                                                }
                                                            } else {
                                                                my $err = $tx->error;
                                                                die "$url error $err->{code} response: $err->{message}" if $err->{code};
                                                                die "$url error Connection error: $err->{message}";
                                                            }
                                                        }
                                                    }
                                                );
                                            }
                                            $content =~ s#<p><strong>Le Copyright Madness vous est offert par</strong>.*##s;
                                            $content =~ s# target="_blank"##gm;
                                            $content =~ s# (class|id|style|width|height|sizes|data-width|data-secret)="[^"]*"##gm;
                                            $content =~ s#<a [^>]*>(<img [^>]*>)</a>#$1#gm;
                                            $content =~ s#(<img [^>]*)>#$1/>#gm;
                                            $content =~ s#(COPYRIGHT|TRADEMARK|PATENT) (MADNESS|WISDOM)#\u\L$1\E \u\L$2\E#gm;
                                            $content =~ s#<figure[^>]*>#<div class="img">#gm;
                                            $content =~ s#</figure>#</div>#gm;
                                            $content =~ s#<figcaption[^>]*>#<div>#gm;
                                            $content =~ s#</figcaption>#</div>#gm;
                                            $url =~ s#.*/([^/]*)#$1#;
                                            say "got #CopyrightMadness";
                                            spurt encode('UTF-8', "$header$content</article>\n$footer"), 'Text/'.$filename;
                                        }
                                    }

                                } else {
                                    my $err = $tx->error;
                                    die "$url error $err->{code} response: $err->{message}" if $err->{code};
                                    die "$url error Connection error: $err->{message}";
                                }
                            }
                        );
                    }
                );
            } else {
                my $err = $tx->error;
                die "$i error $err->{code} response: $err->{message}" if $err->{code};
                die "$i errer Connection error: $err->{message}";
            }
        }
    );
    $i++;
}

Mojo::IOLoop->start unless Mojo::IOLoop->is_running;
